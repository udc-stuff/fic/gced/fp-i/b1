

if __name__ == "__main__":
	t = int(input("Tiempo en segundos: "))

	hours = t // 3600
	minutes = t // 60 - hours * 60
	seconds = t % 60

	print(t, " segundos son ", hours, "h:", minutes, "m:", seconds, "s", sep="")
