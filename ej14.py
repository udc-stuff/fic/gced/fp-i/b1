import datetime

if __name__ == "__main__":
	city = input("Introduzca el nombre de su ciudad: ")
	max_far = int(input("Introduzca la temperatura máxima en grados Fahrenheit: "))
	min_far = int(input("Introduzca la temperatura mínima en grados Fahrenheit: "))

	max_celsius = int( (max_far - 32) * 5/9 )
	min_celsius = int( (min_far - 32) * 5/9 )
	print("----------------------", city, datetime.date.today(), "----------------------")
	print("T Max (ºF)", "T Min (ºF)", "T Max (ºC)", "T Min (ºC)", sep="\t")
	print(max_far, "º F\t\t", min_far, "º F\t\t", max_celsius, "º F\t\t", min_celsius, "º F", sep="")
	print("------------------------------------------------------------------")
