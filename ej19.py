

if __name__ == "__main__":
	IVA_FOOD = 21
	IVA_BOOK = 10

	kg_potatoes = float(input("Kg de patatas comprados: "))
	p_potatoes  = float(input("Precio sin IVA del kilo de patatas (en euros): "))

	kg_apples = float(input("Kg de manzanas comprados: "))
	p_apples  = float(input("Precio sin IVA del kilo de manzanas (en euros): "))

	book = float(input("Importe del libro sin IVA (en euros): "))

	t_potatoes = kg_potatoes * p_potatoes
	t_apples = kg_apples * p_apples

	print("-"*50, "Ticket 1/1")
	print(" Patatas", kg_potatoes, "kg\t", p_potatoes, "€\t", t_potatoes, "€\t", t_potatoes * (1+IVA_FOOD/100), "(IVA", IVA_FOOD, "%)")
	print("Manzanas", kg_apples, "kg\t", p_apples, "€\t", t_apples, "€\t", t_apples * (1+IVA_FOOD/100), "(IVA", IVA_FOOD, "%)")
	print("   Libro", 1, "  \t", book,  "€\t", book, "€\t", book * (1+IVA_BOOK/100), "(IVA", IVA_BOOK, "%)")
	print("-"*62)
	print("TOTAL\t\t\t\t", (t_potatoes + t_apples) * (1+IVA_FOOD/100) + book*(1+IVA_BOOK/100) , "€") 

