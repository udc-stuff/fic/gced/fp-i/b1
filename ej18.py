

if __name__ == "__main__":
	name = input("Introduzca su nombre: ")
	surname = input ("Introduzca sus apellidos: ")

	salary = float(input("Introduzca su sueldo mensual: "))
	leisure = float(input("Introduzca cuanto gasta semanalmente en ocio: "))
	transport = float(input("Introduzca cuanto gasta semanalmente en transporte: "))
	food = float(input("Introduzca cuanto gasta semanalmente en comida: "))

	p_leisure = 100 * (4 * leisure)/salary
	p_food = 100 * (4 * food)/salary
	p_transport = 100 * (4 * transport)/salary
	p_other = 100 - (p_leisure + p_transport + p_food) 

	print("*"*50)
	print("****   ", name, surname)
	print("% ocio\t% comida\t% Transporte\t% Otros")
	print(p_leisure, " %\t", p_food, " %\t", p_transport, " %\t", p_other, " %")
	print("****")
	print("\t\t\t\tGasto semanal")
	print("\t\t\t\t     ", leisure + food + transport, "€")
