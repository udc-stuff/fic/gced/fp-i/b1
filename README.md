# Boletín 1

En este repositorio os encontraréis ejercicios del boletín 1 que se han realizado en clase de prácticas.

Quizás encontréis alguna pequeña modificación con respecto al enunciado.
Recordar que esas pequeñas modificaciones han sido realizadas para explicar cosas concretas que ocurren al usar el lenguaje Python.

## Ejercicio 01.

Plantee e implemente un programa que solicite el nombre y la edad de un usuario por teclado de forma que el ordenador le salude indicándole la edad que tiene.


```
	Introduzca su nombre: Nicolas
	Introduzca su edad: 18
	¡Buenos días Nicolas, disfrute de sus 18 años!
```

PRECONDICIÓN: el usuario introduce correctamente por teclado los datos solicitados.

### Solución

```python
if __name__ == "__main__":
	name = input('Introduce tu nombre: ')
	age = int(input('Introduce tu edad: '))

	print("¡Buenos días ", name, ", disfrute de sus ", age, " años!", sep="")
```

## Ejercicio 02.

Plante e implemente un programa en que solicite el nombre, apellidos y edad de un usuario por teclado y que le muestre el siguiente mensaje por pantalla:

```
	Sr/Sra. Apellido, le faltan aún X años para jubilarse
	Ejemplo: Sr/Sra. Rodríguez, le faltan aún 30 años para jubilarse
```

NOTA: Utilice la edad de jubilación como una constante con valor 67 años.

### Solución

```python
if __name__ == "__main__":
	RETIREMENT = 67

	name = input("Introduzca su nombre: ")
	surname = input("Introduzca su apellido: ")
	age = int(input("Introduzca su edad: "))

	print("Sr/Sra ", surname, ", le faltan ", RETIREMENT - age, " años para jubilarse", sep="")
```

## Ejercicio 03.

Plantee e implemente un programa que solicite por teclado la base y altura de un triángulo rectángulo y muestre por pantalla su superficie. Presente la salida con el siguiente formato:

```
	La superficie del triángulo de base XX.XX y altura YY.YY es ZZ.ZZ
	Ejemplo: La superficie del triángulo de base 6.45 y altura 3.38 es 10.9
```

PRECONDICIÓN: los datos solicitados que se introducen por teclado son correctos.

### Solución

```python
if __name__ == "__main__":
	base = float(input("Base: "))
	height = float(input("Altura: "))

	print("La superficie del triangulo de base", base, "y altura", height, "es", base*height/2)

```

## Ejercicio 04.

Plantee e implemente un programa que solicite por teclado los lados de un rectángulo y muestre por pantalla su perímetro.

```
	El perímetro del rectángulo de base XX.XX y altura YY.YY es ZZ.ZZ
	Ejemplo: El perímetro del rectángulo de base 12.34 y altura 10.01 es 44.5
```

PRECONDICIÓN: los datos solicitados que se introducen por teclado son correctos.

### Solución

```python
if __name__ == "__main__":
	base = float(input("Base: "))
	height = float(input("Altura: "))

	print("El perímetro del rectangulo de base", base, "y altura", height, "es", (base+height)*2)

```

## Ejercicio 05.

Plantee e implemente un programa que solicite por teclado los lados de un rectángulo y muestre por pantalla su superficie.

### Solución

```python
if __name__ == "__main__":
	base = float(input("Base: "))
	height = float(input("Altura: "))

	print("La superficie del rectangulo de base", base, "y altura", height, "es", base * height)

```

## Ejercicio 06.

Plantee e implemente un programa que pida el radio de una esfera y calcule su área y su volumen.

### Solución

```python
import math

if __name__ == "__main__":
	r = float(input("Introduzca el radio de la esfera: "))
	
	a = 4 * math.pi * r ** 2
	v = 4/3 * math.pi * r ** 3

	print("La esfera de radio ", r, " tiene una superficie de ", a, ", y un volumen de ", v, sep="")
```

## Ejercicio 07.

Plantee e implemente un programa que solicite el precio de un producto (sin IVA) y proporcione por pantalla el importe de un producto con IVA incluido.

```
	Precio del producto (sin IVA): 1.34
	El importe total (IVA incluido) es de 1.66 €
```

NOTA: Supón un IVA constante de 24% para todos los productos.

### Solución

```python
if __name__ == "__main__":
	IVA = 1.24

	prize = float(input('Precio del producto (sin IVA): '))

	aux = prize * 1.24
	print("El importe total (IVA incluido) es de ", aux, "€")
```

## Ejercicio 08.

Plantee e implemente un programa que solicite al usuario su nombre, edad y lo que se ha gastado en cervezas y en transporte durante una semana y muestre por pantalla esos mismos datos y la suma de los gastos.

```
Introduzca su nombre: Nicolas
Introduzca su edad: 18
Introduzca el total de sus gastos semanales en cervezas: 24
Introduzca el total de sus gastos semanales en transporte: 11

Nombre: Nicolas
Edad: 18
Gasto semanal en cervezas: 24€
Gasto semanal en transporte: 11€
Total de gastos semanales: 35 €
```

### Solución

```python
if __name__ == "__main__":
	name = input("Introduzca su nombre: ")
	age = int(input("Introduzca su edad: "))
	beer = float(input("Introduzca el total de sus gastos semanales en cervezas: "))
	bus = float(input("Introduzca el total de sus gastos semanales en transporte: "))

	print("Nombre:", name)
	print("Edad:", age)
	print("Gasto semanal en cervezas:", beer)
	print("Gasto semanal en transporte:", bus)
	print("Total de gastos semanales:", beer + bus)
```

## Ejercicio 09.

Plantee e implemente un programa que solicite el nombre, edad, número de hijos y sueldo anual de una persona y muestre por pantalla la misma información indicando su sueldo mensual en vez del sueldo anual.

```
Introduzca su nombre: Nicolas
Introduzca su edad: 18
Introduzca su número de hijos: 3
Introduzca su sueldo anual: 21345.36

Nombre: Nicolas
Edad: 18
Número de hijos: 24€
Sueldo mensual: 1778.78€
```

### Solución

```python
if __name__ == "__main__":
	name = input("Introduzca su nombre: ")
	age = int(input("Introduzca su edad: "))
	children = int(input("Introduzca su número de hijos: "))
	salary = float(input("Introduzca su sueldo anual: "))

	print("Nombre:", name)
	print("Edad:", age)
	print("Número de hijos:", children)
	print("Sueldo mensual: ", salary/12, "€", sep="")

```

## Ejercicio 10.

Plantee e implemente un programa que calcule el producto escalar de dos vectores en el espacio euclídeo.

```
	Datos del primer vector
		Primera coordenada: 2
		Segunda coordenada: 4
		Tercera coordenada: 6

	Datos del segundo vector
		Primera coordenada: 3
		Segunda coordenada: 1
		Tercera coordenada: 9

	Producto escalar: 64
```

### Solución

```python
if __name__ == "__main__":
	print("Datos del primer vector")
	i1 = float(input("\tPrimera coordenada: "))
	i2 = float(input("\tSegunda coordenada: "))
	i3 = float(input("\tTercera coordenada: "))

	print("Datos del segundo vector")
	j1 = float(input("\tPrimera coordenada: "))
	j2 = float(input("\tSegunda coordenada: "))
	j3 = float(input("\tTercera coordenada: "))

	print("\nProducto escalar: ", i1*j1 + i2*j2 + i3*j3)

```

## Ejercicio 11.

Plantee e implemente un programa que convierta un tiempo expresado en segundos al formato horas:minutos:segundos.

```
Tiempo en segundos: 3750
3750 segundos son 1h:2m:30s
```

### Solución

```python
if __name__ == "__main__":
	t = int(input("Tiempo en segundos: "))

	hours = t // 3600
	minutes = t // 60 - hours * 60
	seconds = t % 60

	print(t, " segundos son ", hours, "h:", minutes, "m:", seconds, "s", sep="")
```

## Ejercicio 12.

Plantee e implemente un programa que muestre por pantalla las 5 primeras filas de
un triángulo de Floyd.

```
	1
	2 3
	4 5 6
	7 8 9 10
	11 12 13 14 15
```

NOTA: No se deben utilizar bucles en este ejercicio, simplemente sentencias print con sus correspondientes descriptores de formato (no utilice espacios en blanco para separar los números).


### Solución

```python
if __name__ == "__main__":
	print("1")
	print("2\t3")
	print("4\t5\t6")
	print("7\t8\t9\t10")
	print("11\t12\t13\t14\t15")
```

## Ejercicio 13.

Plantee e implemente un programa que muestre por pantalla una tabla indicando el radio, el perímetro y el área de tres círculos cuyo radio es solicitado por teclado.

```
	RADIO PERIMETRO AREA
	===== ========= =====
	2 12.56 12.56
	3 18.86 28.27
	4 25.13 50.26
```

### Solución

```python
import math

if __name__ == "__main__":
	r1 = int(input("Introduce el radio de la primera circunferencia: "))
	r2 = int(input("Introduce el radio de la segunda circunferencia: "))
	r3 = int(input("Introduce el radio de la tercera circunferencia: "))

	print("RADIO\tPERIMETRO\t\tAREA")
	print("=====\t=========\t\t====")
	print(r1, 2*math.pi*r1, math.pi*r1**2, sep="\t")
	print(r2, 2*math.pi*r2, math.pi*r2**2, sep="\t")
	print(r3, 2*math.pi*r3, math.pi*r3**2, sep="\t")

```

## Ejercicio 14.

Plantee e implemente un programa que a partir de la temperatura máxima y mínima diaria de una ciudad en grados Fahrenheit (introducidas por teclado) proporcione dichas temperaturas en grados centígrados por pantalla.

```
Introduzca el nombre de su ciudad: Lugo
Introduzca la temperatura máxima en grados Fahrenheit: 76
Introduzca la temperatura mínima en grados Fahrenheit: 57
-------------------- Lugo 12/9/2019 --------------------
T Max (ºF) T Min (ºF) T Max (ºC) T Min (ºC)
76 ºF 57 ºF 24.44 ºC 13.88 ºC
--------------------------------------------------------
```

### Solución

```python
import datetime

if __name__ == "__main__":
	city = input("Introduzca el nombre de su ciudad: ")
	max_far = int(input("Introduzca la temperatura máxima en grados Fahrenheit: "))
	min_far = int(input("Introduzca la temperatura mínima en grados Fahrenheit: "))

	max_celsius = int( (max_far - 32) * 5/9 )
	min_celsius = int( (min_far - 32) * 5/9 )
	print("----------------------", city, datetime.date.today(), "----------------------")
	print("T Max (ºF)", "T Min (ºF)", "T Max (ºC)", "T Min (ºC)", sep="\t")
	print(max_far, "º F\t\t", min_far, "º F\t\t", max_celsius, "º F\t\t", min_celsius, "º F", sep="")
	print("------------------------------------------------------------------")
```

## Ejercicio 15.

Un camión transporta T kilogramos de baldosas y se sabe que cada baldosa pesa B kilogramos. Plantee e implemente un programa que indique el número de baldosas que puede transportar el camión.

NOTA: T y B son datos que introduce el usuario por teclado.

### Solución

```python
if __name__ == "__main__":
	t = float(input("Kilogramos que transporta el camión: "))
	b = float(input("Kilogramos que pesa una baldosa: "))

	result = int(t / b)

	print("El camión puede transportar", result, "baldosas")
```

## Ejercicio 16.

Plantee e implemente un programa para calcular la suma de dos matrices 2x2. Pide los datos oportunos y calcule el resultado deseado, mostrándolo después en pantalla.

### Solución

```python
if __name__ == "__main__":
	print("Matriz 1")
	m11 = int(input("\tValor de la posición (1, 1): "))
	m12 = int(input("\tValor de la posición (1, 2): "))
	m21 = int(input("\tValor de la posición (2, 1): "))
	m22 = int(input("\tValor de la posición (2, 2): "))


	print("Matriz 2")
	t11 = int(input("\tValor de la posición (1, 1): "))
	t12 = int(input("\tValor de la posición (1, 2): "))
	t21 = int(input("\tValor de la posición (2, 1): "))
	t22 = int(input("\tValor de la posición (2, 2): "))

	print("Resultado:")
	print(m11+t11, m12+t12, sep="\t")
	print(m21+t21, m22+t22, sep="\t")
	
```

## Ejercicio 17.

Plantee e implemente un programa para calcular el producto de dos matrices 2x2. Pide los datos oportunos y calcule el resultado deseado, mostrándolo después en pantalla.

### Solución

```python
if __name__ == "__main__":
	print("Matriz 1")
	m11 = int(input("\tValor de la posición (1, 1): "))
	m12 = int(input("\tValor de la posición (1, 2): "))
	m21 = int(input("\tValor de la posición (2, 1): "))
	m22 = int(input("\tValor de la posición (2, 2): "))


	print("Matriz 2")
	t11 = int(input("\tValor de la posición (1, 1): "))
	t12 = int(input("\tValor de la posición (1, 2): "))
	t21 = int(input("\tValor de la posición (2, 1): "))
	t22 = int(input("\tValor de la posición (2, 2): "))

	print("Resultado:")
	print(m11*t11 + m12*t21, m11*t12 + m12*t22, sep="\t")
	print(m21*t11 + m22*t21, m21*t12 + m22*t22, sep="\t")

```

## Ejercicio 18.

Plantee e implemente un programa que solicite a un usuario su nombre, apellidos,
sueldo mensual y el dinero que casta diariamente en ocio, comida y transporte. A
continuación, debe mostrar una tabla como la que se indica donde se recoja el % de su
sueldo que gasta semanalmente en ocio, comida y transporte. Incluya también el total
del gasto semanal realizado.

```
	*********************************************
	****          Nicolas Rodriguez          ****
	% Ocio    % Comida    % Transporte    % Otros
	  7.5%      5%          10.34         % 2.16%
	****                                     ****
	                                Gasto semanal
	                                      250€
```

### Solución

```python
if __name__ == "__main__":
	name = input("Introduzca su nombre: ")
	surname = input ("Introduzca sus apellidos: ")

	salary = float(input("Introduzca su sueldo mensual: "))
	leisure = float(input("Introduzca cuanto gasta semanalmente en ocio: "))
	transport = float(input("Introduzca cuanto gasta semanalmente en transporte: "))
	food = float(input("Introduzca cuanto gasta semanalmente en comida: "))

	p_leisure = 100 * (4 * leisure)/salary
	p_food = 100 * (4 * food)/salary
	p_transport = 100 * (4 * transport)/salary
	p_other = 100 - (p_leisure + p_transport + p_food) 

	print("*"*50)
	print("****   ", name, surname)
	print("% ocio\t% comida\t% Transporte\t% Otros")
	print(p_leisure, " %\t", p_food, " %\t", p_transport, " %\t", p_other, " %")
	print("****")
	print("\t\t\t\tGasto semanal")
	print("\t\t\t\t     ", leisure + food + transport, "€")
```

## Ejercicio 19.

Plantee e implemente un programa para controlar la compra diaria. El programa debe pedir los kilos de patatas comprados y su precio sin IVA, los kilos de manzanas comprados y su precio in IVA y cuánto ha pagado por un libro que se ha comprado sin
IVA. Suponga que el IVA de los productos alimenticios es del 21% y de los productos de cultura 10%. Muestre por pantalla el importe de cada producto y el importe total de la
compra.

```
	Kg de patatas comprados: 7
	Precio sin IVA del kilo de patatas (en euros): 0.69
	Kg de manzanas comprados: 4
	Precio sin IVA del kilo de patatas (en euros): 1.23
	Importe del libro sin IVA (euros): 19

	----------------------------------- Ticket 1/1
	Patatas 	7 kg 	0.69€	4.83€ 	 5.84€ (IVA 21%)
	Manzanas 	4 kg 	1.23€	4.92€ 	 5.95€ (IVA 21%)
	Libro   	1    	  19€	  19€ 	20.90€ (IVA 10%)
	----------------------------------------------
	TOTAL                         32.69€
```

### Solución

```python
if __name__ == "__main__":
	IVA_FOOD = 21
	IVA_BOOK = 10

	kg_potatoes = float(input("Kg de patatas comprados: "))
	p_potatoes  = float(input("Precio sin IVA del kilo de patatas (en euros): "))

	kg_apples = float(input("Kg de manzanas comprados: "))
	p_apples  = float(input("Precio sin IVA del kilo de manzanas (en euros): "))

	book = float(input("Importe del libro sin IVA (en euros): "))

	t_potatoes = kg_potatoes * p_potatoes
	t_apples = kg_apples * p_apples

	print("-"*50, "Ticket 1/1")
	print(" Patatas", kg_potatoes, "kg\t", p_potatoes, "€\t", t_potatoes, "€\t", t_potatoes * (1+IVA_FOOD/100), "(IVA", IVA_FOOD, "%)")
	print("Manzanas", kg_apples, "kg\t", p_apples, "€\t", t_apples, "€\t", t_apples * (1+IVA_FOOD/100), "(IVA", IVA_FOOD, "%)")
	print("   Libro", 1, "  \t", book,  "€\t", book, "€\t", book * (1+IVA_BOOK/100), "(IVA", IVA_BOOK, "%)")
	print("-"*62)
	print("TOTAL\t\t\t\t", (t_potatoes + t_apples) * (1+IVA_FOOD/100) + book*(1+IVA_BOOK/100) , "€") 

```
