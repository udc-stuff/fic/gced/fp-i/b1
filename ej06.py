import math

if __name__ == "__main__":
	r = float(input("Introduzca el radio de la esfera: "))
	
	a = 4 * math.pi * r ** 2
	v = 4/3 * math.pi * r ** 3

	print("La esfera de radio ", r, " tiene una superficie de ", a, ", y un volumen de ", v, sep="")
