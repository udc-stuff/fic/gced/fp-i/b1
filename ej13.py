import math

if __name__ == "__main__":
	r1 = int(input("Introduce el radio de la primera circunferencia: "))
	r2 = int(input("Introduce el radio de la segunda circunferencia: "))
	r3 = int(input("Introduce el radio de la tercera circunferencia: "))

	print("RADIO\tPERIMETRO\t\tAREA")
	print("=====\t=========\t\t====")
	print(r1, 2*math.pi*r1, math.pi*r1**2, sep="\t")
	print(r2, 2*math.pi*r2, math.pi*r2**2, sep="\t")
	print(r3, 2*math.pi*r3, math.pi*r3**2, sep="\t")

